package db

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

func Open(stage string, dbName string, schema string, user string, password string, dbLog bool) (db *gorm.DB, err error) {
	if "prod" != stage {
		dbName = fmt.Sprintf("%s_%s", dbName, stage)
		user = dbName
		password = fmt.Sprintf(".%s!", dbName)
	}

	host := "amidb.cluster-ckdozklnbtt3.ap-southeast-1.rds.amazonaws.com"

	db, err = gorm.Open("postgres", fmt.Sprintf("user=%s dbname=%s password=%s host=%s port=5432 sslmode=disable search_path=%s TimeZone=Asia/Jakarta", user, dbName, password, host, schema))
	if nil != err {
		return
	}

	if "dev" == stage {
		dbLog = true
	}

	db.LogMode(dbLog)

	return
}

func Begin(gormDb *gorm.DB) (tx *gorm.DB) {
	tx = gormDb.Begin()

	return
}

func Rollback(tx *gorm.DB) {
	tx = tx.Rollback()
}

func Commit(tx *gorm.DB) {
	tx = tx.Commit()
}
